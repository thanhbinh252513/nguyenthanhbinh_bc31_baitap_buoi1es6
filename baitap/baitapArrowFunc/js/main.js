const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
let container = document.getElementById("colorContainer");
let colorButton = document.getElementsByClassName("color-button");
let house = document.getElementById("house");
const renderButton = () => {
  let contentHTML = "";
  for (let i = 0; i < colorList.length; i++) {
    const color = colorList[i];
    contentHTML += `<button class="btn color-button ${color}" onclick="chaneColor('${color}')"></button>`;
  }
  document.getElementById("colorContainer").innerHTML = contentHTML;
};
renderButton();

const changeColor = (color, index) => {
  for (let color = 0; color < colorButton.length; color++)
    colorButton[color].classList.remove("active");
  colorButton[index].classList.add("active");
  house.className = "house " + color;
};

for (let i = 0; i < colorButton.length; i++)
  colorButton[i].addEventListener("click", () => {
    changeColor(colorList[i], i);
  });
